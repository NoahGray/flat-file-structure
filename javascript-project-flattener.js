/* 
  This was made to run in a React project's /components folder and turn `SomeComponent/index.js` into SomeComponent.js.

  It's not great code and some small changes still needed to be made to fix all cases.

  Hopefully it can help get you started, and contributions welcome.
*/

import fs from 'fs'
import path from 'path'

const folders = fs.readdirSync(__dirname, {
  withFileTypes: true,
})

folders.forEach(folder => {
  if (folder.isDirectory()) {
    const files = fs.readdirSync(folder.name, {
      withFileTypes: true,
    })

    files.forEach(file => {
      const fileNameParts = file.name.split('.')
      const fileNameSuffix = fileNameParts[0].replace(folder.name, '')

      if (file.isFile()) {
        const filePath = path.resolve(`${folder.name}/${file.name}`)

        const fileDataBuffer = fs.readFileSync(filePath)
        const fileData = fileDataBuffer.toString()

        const newFileData = fileData.replace("from './index'", `from './${folder.name}'`)

        if (fileNameSuffix === 'index') {
          const newFilePath = `${folder.name}.${fileNameParts[1]}${
            fileNameParts.length === 3 ? `.${fileNameParts[2]}` : ''
          }`
          fs.writeFileSync(newFilePath, newFileData)
        } else if (fileNameParts[0] === folder.name) {
          const newFilePath = `${fileNameParts[0]}.${fileNameParts[1]}${
            fileNameParts.length === 3 ? `.${fileNameParts[2]}` : ''
          }`
          fs.writeFileSync(newFilePath, newFileData)
        } else if (fileNameSuffix !== 'index') {
          const dashed = fileNameSuffix.length !== 0 ? `${folder.name}-${fileNameSuffix}` : fileNameParts[0]
          const shouldHaveExtension = ['stories', 'development', 'test', 'ios', 'web', 'android'].includes(file.name)
          const newFilePath = `${dashed}${
            shouldHaveExtension ? fileNameParts[1] + '.' + fileNameParts[2] : '.' + fileNameParts[1]
          }`
          fs.writeFileSync(newFilePath, newFileData)
        }
      }
    })
  }
})
