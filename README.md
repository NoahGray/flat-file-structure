# Flat file structure

This repo talks about an approach to file structure that I've been using in React projects. It's key differentiator is that it avoids folders until they become clearly necessary.

**Example**:

```
src
├── components
│   ├── ActionItem.js
│   ├── ActionItem.stories.js
│   ├── ActionList.js
│   ├── ActionList.web.js
│   ├── ActivityIndicator.js
│   ├── App.js
│   ├── Button-styles.js
│   ├── Button.js
│   ├── Calendar.js
│   ├── Card.js
│   └── ContactDetails.js
```

*Non-flat example:*

```
src
├── components
│   ├── App
│   │   └── index.js
│   ├── ChangePassword
│   │   ├── PasswordForm.js
│   │   ├── index.js
│   │   └── styles.js
│   ├── Profile
│   │   ├── index.js
│   │   └── index.web.js
│   └── _core
│       ├── ActionItem
│       │   ├── ActionItem.stories.js
│       │   └── index.js
│       ├── ActionList
│       │   ├── index.js
│       │   └── index.web.js
│       ├── ActivityIndicator
│       │   └── index.js
│       ├── Button
│       │   ├── button.component.js
│       │   ├── index.js
│       │   └── styles.js
│       ├── Calendar
│       │   └── index.js
│       ├── Card
│       │   └── index.js
│       ├── ContactDetails
│       │   ├── ContactDetails.stories.js
│       │   └── index.js
```

## Tools

There is an imperfect Node helper script in [javascript-project-flattener.js](javascript-project-flattener.js). It can be run with `node -r esm ./javascript-project-flattener.js`.

## Plans

I hope to have time to explain my workflow with this better and explain it's advantages.
